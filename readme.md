
# Microservice Deploy in a K8s Cluster
Setup Microservice Web Store App in a mangaed Linode Kubernetes cluster.

## Technologies Used
- Kubernetes
- Helm
- Linode Manged K8s Cluster

## Project Description
- Deploy Microservice Web Store in local Kubernetes cluster using a Helm chart

## Prerequisites
- Linode Account
- [Sample Project](https://github.com/techworld-with-nana/microservices-demo)
- [Config Files](https://gitlab.com/twn-devops-bootcamp/latest/10-kubernetes/helm-chart-microservices/-/blob/starting-code/config.yaml?ref_type=heads)

## Guide Steps
### Create Deployment & Service Configurations
We have **11** Microservices, each will have a **Deployment** and **Service**. We will fill in the **xxxx** on each line to properly configure them.

We will start by editing the `config.yaml` obtained from the [Config Files](https://gitlab.com/twn-devops-bootcamp/latest/10-kubernetes/helm-chart-microservices/-/blob/starting-code/config.yaml?ref_type=heads) repository

Example for the **emailservice**:
- We utilize labels to connect the **Deployment** and **Service** together
- We set the location of the image to use
- We set resource requirements for the service to run
- We configure ports to allow for service communications
- Set an environment variable for the port (Required for emailservice)
```yaml
---
apiVersion:  apps/v1
kind:  Deployment
metadata:
 name:  emailservice #Name
spec:
 selector:
 matchLabels:
 app:  emailservice #Label
 template:
 metadata:
 labels:
 app:  emailservice #Label
 spec:
 containers:
 -  name:  service
 image:  gcr.io/google-samples/microservices-demo/emailservice:v0.8.0 #What image for this service
 ports:
 -  containerPort:  8080 #Where this service runs
 env:
 -  name:  PORT
 value:  "8080"
 livenessProbe:
 grpc:
 port:  8080
 periodSeconds:  5
 readinessProbe:
 grpc:
 port:  8080
 periodSeconds:  5
 resources:
 requests: 
 cpu:  100m
 memory:  64Mi
 limits:
 cpu:  200m
 memory:  128Mi
---
apiVersion:  v1
kind:  Service
metadata:
 name:  emailservice #Name
spec:
 type:  ClusterIP
 selector:
 app:  emailservice #Label to Match Deployment
 ports:
 -  protocol:  TCP
 port:  5000
 targetPort:  8080
```

### Deploy Microservices into a K8s Cluster
- Log in to Linode and Create a new managed K8s Cluster
	- Cluster Label: **online-shop-microservices**
	- Region: **CLOSEST_TO_YOU**
	- K8s Version: **1.28**
	- HA Control Plane: **No**
	- Node Pool: **Shared CPU | 3x Linode 2 GB**
- Download the kubeconfig yaml from the Linode portal
- Modify file permissions to be more strict
	- `chmod 400 online-shop-microservices-kubeconfig.yaml`
- Set the KUBECONFIG environment variable
	- `export KUBECONFIG=/home/USER/online-shop-microservices-kubeconfig.yaml`
- Test connection
	- `kubectl get node`

![Successful Connection](/images/m10-5-successful-connection.png)

- Create a namespace
	- `kubectl create ns microservices`
- Deploy Microservices into the namespace
	- `kubectl apply -f config.yaml -n microservices`
- Verify Microservices are running
	- `kubectl get pod -n microservices`
- Verify the website is reachable from your web browser
	- Use the external IP of the service/frontend

![Web Store Accessible](/images/m10-5-store-accessible-via-web-browser.png)

![Shopping Cart Test](/images/m10-5-shopping-cart-test.png)